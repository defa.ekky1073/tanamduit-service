SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for invest_product_mutual_fund
-- ----------------------------
DROP TABLE IF EXISTS `invest_product_mutual_fund`;
CREATE TABLE `invest_product_mutual_fund`
(
  `id`                         bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_asset_class`             int(11) UNSIGNED    NOT NULL,
  `id_partner`                 bigint(20) UNSIGNED NOT NULL,
  `id_ref`                     bigint(20) UNSIGNED NOT NULL,
  `fund_fact_url`              text                NULL     DEFAULT NULL,
  `prospectus_url`             text                NULL     DEFAULT NULL,
  `return_percentage`          decimal(5, 2)       NULL     DEFAULT NULL,
  `min_first_buy`              decimal(20, 2)      NULL     DEFAULT NULL,
  `min_buy`                    decimal(20, 2)      NULL     DEFAULT NULL,
  `min_sell`                   decimal(20, 2)      NULL     DEFAULT NULL,
  `min_balance_unit`           decimal(20, 2)      NULL     DEFAULT NULL,
  `min_balance_amount`         decimal(20, 2)      NULL     DEFAULT NULL,
  `min_redeem_transfer_period` int(11)             NULL     DEFAULT NULL,
  `max_redeem_transfer_period` int(11)             NULL     DEFAULT NULL,
  `nav_date`                   date                NULL     DEFAULT NULL,
  `buy_price`                  decimal(20, 6)      NULL     DEFAULT NULL,
  `sell_price`                 decimal(20, 6)      NULL     DEFAULT NULL,
  `aum`                        decimal(20, 2)      NULL     DEFAULT NULL,
  `one_month_performance`      decimal(9, 2)       NULL     DEFAULT NULL,
  `three_month_performance`    decimal(9, 2)       NULL     DEFAULT NULL,
  `six_month_performance`      decimal(9, 2)       NULL     DEFAULT NULL,
  `year_performance`           decimal(9, 2)       NULL     DEFAULT NULL,
  `year_to_date_performance`   decimal(9, 2)       NULL     DEFAULT NULL,
  `inception_performance`      decimal(9, 2)       NULL     DEFAULT NULL,
  `five_year_performance`      decimal(9, 2)       NULL     DEFAULT NULL,
  `created_time`               datetime(0)         NULL     DEFAULT NULL,
  `last_modified_time`         datetime(0)         NULL     DEFAULT NULL,
  `deleted_time`               datetime(0)         NULL     DEFAULT NULL,
  `is_deleted`                 tinyint(1)          NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_asset_class` (`id_asset_class`) USING BTREE,
  INDEX `id_partner` (`id_partner`) USING BTREE,
  INDEX `id_ref` (`id_ref`) USING BTREE,
  INDEX `is_deleted` (`is_deleted`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for invest_product_mutual_fund_disburse_bank
-- ----------------------------
DROP TABLE IF EXISTS `invest_product_mutual_fund_disburse_bank`;
CREATE TABLE `invest_product_mutual_fund_disburse_bank`
(
  `id`                 bigint(20) UNSIGNED                                     NOT NULL AUTO_INCREMENT,
  `id_product`         bigint(20) UNSIGNED                                     NOT NULL,
  `id_bank`            int(11)                                                 NOT NULL,
  `ksei_code`          varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL     DEFAULT NULL,
  `bank_name`          varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL     DEFAULT NULL,
  `account_number`     varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL     DEFAULT NULL,
  `account_name`       varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL     DEFAULT NULL,
  `bank_image_url`     text CHARACTER SET utf8 COLLATE utf8_general_ci         NULL     DEFAULT NULL,
  `is_default`         tinyint(1)                                              NOT NULL DEFAULT 0,
  `created_time`       datetime(0)                                             NULL     DEFAULT NULL,
  `last_modified_time` datetime(0)                                             NULL     DEFAULT NULL,
  `deleted_time`       datetime(0)                                             NULL     DEFAULT NULL,
  `is_deleted`         tinyint(1)                                              NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_product` (`id_product`) USING BTREE,
  INDEX `is_deleted` (`is_deleted`) USING BTREE,
  INDEX `id_bank` (`id_bank`) USING BTREE,
  INDEX `is_default` (`is_default`) USING BTREE,
  FULLTEXT INDEX `ksei_code` (`ksei_code`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for invest_product_mutual_fund_history
-- ----------------------------
DROP TABLE IF EXISTS `invest_product_mutual_fund_history`;
CREATE TABLE `invest_product_mutual_fund_history`
(
  `id`                         bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product`                 bigint(20) UNSIGNED NOT NULL,
  `id_asset_class`             int(11)    UNSIGNED NOT NULL,
  `id_partner`                 bigint(20) UNSIGNED NOT NULL,
  `id_ref`                     bigint(20) UNSIGNED NOT NULL,
  `return_percentage`          decimal(5, 2)       NULL     DEFAULT NULL,
  `min_first_buy`              decimal(20, 2)      NULL     DEFAULT NULL,
  `min_buy`                    decimal(20, 2)      NULL     DEFAULT NULL,
  `min_sell`                   decimal(20, 2)      NULL     DEFAULT NULL,
  `min_balance_unit`           decimal(20, 2)      NULL     DEFAULT NULL,
  `min_balance_amount`         decimal(20, 2)      NULL     DEFAULT NULL,
  `min_redeem_transfer_period` int(11)             NULL     DEFAULT NULL,
  `max_redeem_transfer_period` int(11)             NULL     DEFAULT NULL,
  `nav_date`                   date                NULL     DEFAULT NULL,
  `buy_price`                  decimal(20, 6)      NULL     DEFAULT NULL,
  `sell_price`                 decimal(20, 6)      NULL     DEFAULT NULL,
  `aum`                        decimal(20, 2)      NULL     DEFAULT NULL,
  `one_month_performance`      decimal(9, 2)       NULL     DEFAULT NULL,
  `three_month_performance`    decimal(9, 2)       NULL     DEFAULT NULL,
  `six_month_performance`      decimal(9, 2)       NULL     DEFAULT NULL,
  `year_performance`           decimal(9, 2)       NULL     DEFAULT NULL,
  `year_to_date_performance`   decimal(9, 2)       NULL     DEFAULT NULL,
  `inception_performance`      decimal(9, 2)       NULL     DEFAULT NULL,
  `five_year_performance`      decimal(9, 2)       NULL     DEFAULT NULL,
  `created_time`               datetime(0)         NULL     DEFAULT NULL,
  `last_modified_time`         datetime(0)         NULL     DEFAULT NULL,
  `deleted_time`               datetime(0)         NULL     DEFAULT NULL,
  `is_deleted`                 tinyint(1)          NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_product` (`id_product`) USING BTREE,
  INDEX `id_partner` (`id_partner`) USING BTREE,
  INDEX `id_ref` (`id_ref`) USING BTREE,
  INDEX `is_deleted` (`is_deleted`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  CHARACTER SET = latin1
  COLLATE = latin1_swedish_ci
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for invest_product_general
-- ----------------------------
DROP TABLE IF EXISTS `invest_product_general`;
CREATE TABLE `invest_product_general`
(
  `id`                 bigint(20) UNSIGNED                                     NOT NULL AUTO_INCREMENT,
  `id_asset_class`     int(11) UNSIGNED                                        NOT NULL,
  `id_ref`             bigint(20) UNSIGNED                                     NOT NULL,
  `code`               varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name`               varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `abbreviation`       varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL     DEFAULT NULL,
  `type`               varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'MUTUALFUND / GOLD / OBLIGASI / DEPOSITO',
  `image_url`          text CHARACTER SET utf8 COLLATE utf8_general_ci        NULL     DEFAULT NULL,
  `description`        mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci  NULL,
  `is_active`          tinyint(1)                                             NOT NULL DEFAULT 0,
  `is_sharia`          tinyint(1)                                             NOT NULL DEFAULT 0,
  `created_time`       datetime(0)                                            NULL     DEFAULT NULL,
  `last_modified_time` datetime(0)                                            NULL     DEFAULT NULL,
  `deleted_time`       datetime(0)                                            NULL     DEFAULT NULL,
  `is_deleted`         tinyint(1)                                             NULL     DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `code_2` (`code`) USING BTREE,
  INDEX `is_active` (`is_active`) USING BTREE,
  INDEX `is_sharia` (`is_sharia`) USING BTREE,
  INDEX `is_deleted` (`is_deleted`) USING BTREE,
  FULLTEXT INDEX `code` (`code`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for invest_product_asset_class
-- ----------------------------
DROP TABLE IF EXISTS `invest_product_asset_class`;
CREATE TABLE `invest_product_asset_class`
(
  `id`                 bigint(20) UNSIGNED                                     NOT NULL AUTO_INCREMENT,
  `name`               varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `return_investment`  decimal(20,2)                                          NULL     DEFAULT NULL,
  `is_active`          tinyint(1)                                             NOT NULL DEFAULT 1,
  `parent_id`          bigint(20) UNSIGNED                                    NOT NULL,
  `created_time`       datetime(0)                                            NULL     DEFAULT NULL,
  `last_modified_time` datetime(0)                                            NULL     DEFAULT NULL,
  `deleted_time`       datetime(0)                                            NULL     DEFAULT NULL,
  `is_deleted`         tinyint(1)                                             NULL     DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `is_active` (`is_active`) USING BTREE,
  INDEX `is_deleted` (`is_deleted`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
