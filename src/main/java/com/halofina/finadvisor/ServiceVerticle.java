package com.halofina.finadvisor;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import com.halofina.finadvisor.utils.kafka.JsonParseException;
import com.halofina.finadvisor.utils.kafka.JsonParser;
import com.halofina.finadvisor.utils.kafka.KafkaPackage;
import com.halofina.finadvisor.utils.kafka.KafkaUtils;

import id.halofina.roboadvisor.core.RoboEventBusVerticle;
import id.halofina.roboadvisor.core.database.driver.ClientFactory;
import id.halofina.roboadvisor.integration.investment.tanamduit.verticle.TanamduitEventBusVerticle;
import id.halofina.roboadvisor.module.investment.product.general.model.GeneralModel;
import id.halofina.roboadvisor.module.investment.product.mutualfund.model.MutualFundModel;
import id.halofina.roboadvisor.module.investment.product.mutualfund.pojo.MutualFund;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;

public class ServiceVerticle extends AbstractVerticle {
    public static EventBus eventBus;

    @Override
    public void start(Future<Void> fut) throws URISyntaxException {
        String port = getPort();

        URI dbUri = new URI(System.getenv("ROBO_INTEGRATION_TANAMDUIT_DB"));

        JsonObject webConfig = new JsonObject().put("url", "hapidev.tanamduit.com")
            .put("consumerKey", "nTNChMF4WBrxVi9juURMNBV3ufQa")
            .put("consumerSecret", "yzpN9XWLP7_ejZs22vRjuxFhtnga")
            .put("app_secret", "90bfc7632c0ed3575995da61e78cc050").put("app_key", "25990207");


        vertx.deployVerticle(new TanamduitEventBusVerticle(vertx, dbUri, webConfig), deployHandler -> {
            if (deployHandler.succeeded()) {
                System.out.println("Tanamduit Event Bus Verticle Deployed");

                this.eventBus = vertx.eventBus();
            } else {
                System.out.println("Tanamduit Event Bus Verticle Deployment Failed, cause: ");
                System.out.println(deployHandler.cause());
            }
        });

        Router router = mainRouter(vertx);

        vertx.createHttpServer().requestHandler(router).listen(Integer.valueOf(port));

        fut.complete();
    }

    private String getPort() {
        // String CONFIG_HTTP_PORT = "FINADVISOR_HTTP_PORT";
        // return System.getenv(CONFIG_HTTP_PORT) != null ? System.getenv(CONFIG_HTTP_PORT) :  (System.getenv("PORT") != null ? System.getenv("PORT") : "8080");
        return "8088";
    }

    private Router mainRouter(Vertx vertx) {
        Router router = Router.router(vertx);

        router.route().handler(BodyHandler.create());

        router.route()
            .handler(CorsHandler.create("*")
                .allowedMethod(HttpMethod.GET).allowedMethod(HttpMethod.POST).allowedMethod(HttpMethod.OPTIONS)
                .allowedHeader("Access-Control-Allow-Method")
                .allowedHeader("Access-Control-Allow-Origin")
                .allowedHeader("Access-Control-Allow-Credentials")
                .allowedHeader("origin").allowedHeader("Content-Type"));
                    
        router.mountSubRouter("/tanamduit", tanamduitRouter(vertx));

        return router;
    }

    private Router tanamduitRouter(Vertx vertx) {
        Router router = Router.router(vertx);

        router.route(HttpMethod.GET, "/getProductList").handler(this::getProduct);

        return router;
    }

    private void getProduct(RoutingContext routingContext) {
        HttpServerResponse response = routingContext.response();

        JsonObject data = new JsonObject();
        JsonObject res = new JsonObject();
        
        eventBus.send(TanamduitEventBusVerticle.EVENT_GET_PRODUCT_LIST, data, sendHandler -> {
            if (sendHandler.succeeded()) {
                res.put("s", true);
                res.put("c", 200);
                res.put("d", new JsonObject(sendHandler.result().body().toString()));
            } else {
                res.put("s", false);
                res.put("c", 500);
                res.put("m", sendHandler.cause().getMessage());
            }

            response.end(res.toString());
        });
    }
}