package com.halofina.finadvisor;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import com.halofina.finadvisor.utils.kafka.JsonParseException;
import com.halofina.finadvisor.utils.kafka.JsonParser;
import com.halofina.finadvisor.utils.kafka.KafkaPackage;
import com.halofina.finadvisor.utils.kafka.KafkaUtils;

import id.halofina.roboadvisor.core.database.driver.ClientFactory;
import id.halofina.roboadvisor.integration.investment.tanamduit.verticle.TanamduitEventBusVerticle;
import id.halofina.roboadvisor.module.investment.product.general.model.GeneralModel;
import id.halofina.roboadvisor.module.investment.product.mutualfund.model.MutualFundModel;
import id.halofina.roboadvisor.module.investment.product.mutualfund.pojo.MutualFund;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;

public class ConsumerVerticle extends AbstractVerticle {
    private static EventBus eventBus = ServiceVerticle.eventBus;
    private static JDBCClient jdbcClient;
    private static MutualFundModel mutualFundModel;
    private static GeneralModel generalModel;
    private static String url = "ec2-18-136-123-232.ap-southeast-1.compute.amazonaws.com:9092";

    public static final String TOPIC_UPDATE_PRODUCT_NAV = "halofina.roboadvisor.integration.investment.tanamduit.UPDATEPRODUCTNAV";
    public static final String groupId = "tanamduit";

    public void start(Future<Void> future) throws URISyntaxException {

        URI dbUri = new URI(System.getenv("ROBO_INTEGRATION_TANAMDUIT_DB"));

        jdbcClient = ClientFactory.createShared(vertx, dbUri, ClientFactory.Driver.MARIADB);

        mutualFundModel = new MutualFundModel(jdbcClient);
        generalModel = new GeneralModel(jdbcClient);

        KafkaUtils.consume(vertx, url, TOPIC_UPDATE_PRODUCT_NAV, groupId, consumeHandler -> {
            System.out.println("Message Received");
            if(consumeHandler.result().key().equalsIgnoreCase(KafkaPackage.Type.JSON.toString())) {
                try {
                    JsonObject payload = JsonParser.string2Json(consumeHandler.result());
                    updateProductNav(payload.getString("product_code"));
                } catch (JsonParseException e) {
                    System.out.println(e.getMessage());
                }
            }
        });
    }

    public void updateProductNav(String productCode) {
        
        JsonObject data = new JsonObject()
            .put("product_code", productCode);

        eventBus.send(TanamduitEventBusVerticle.EVENT_GET_PRODUCT_NAV_HISTORY, data, sendHandler -> {
            if (sendHandler.succeeded()) {
                JsonObject res = new JsonObject(sendHandler.result().body().toString());

                JsonArray resArray = res.getJsonArray("response");
                JsonObject lastData = resArray.getJsonObject(resArray.size()-1);

                Map<String, Object> filter = new HashMap<>();
                filter.put(GeneralModel.CODE, productCode);

                generalModel.find(filter, null, null, null, findHandler -> {
                    if(findHandler.succeeded()) {
                        Long id = findHandler.result().get(0).getId();

                        MutualFund updateData = new MutualFund();
                        updateData.setId(id);
                        updateData.setNavDate(LocalDateTime.parse(lastData.getString("nav_date")));
                        updateData.setBuyPrice(Double.parseDouble(lastData.getInteger("nav_value").toString()));
                        updateData.setSellPrice(Double.parseDouble(lastData.getInteger("nav_value").toString()));
                        
                        mutualFundModel.update(updateData, updateHandler -> {
                            if (updateHandler.succeeded()) {
                                System.out.println("Update for product ID: " + id + " success");
                            } else {
                                System.out.println("Update for product ID: " + id + " failed, cause: " + updateHandler.cause());
                            }
                        });
                    } else {
                        System.out.println("Product Update failed, data not found");
                    }
                });
            } else {
                System.out.println("GET PRODUCT NAV HISTORY Event Bus Failed, cause: ");
                System.out.println(sendHandler.cause());
            }
        });
    }
}