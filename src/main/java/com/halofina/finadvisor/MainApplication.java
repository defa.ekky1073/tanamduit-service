package com.halofina.finadvisor;

import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class MainApplication {
    public static void main(String[] args) {
        Logger log = LoggerFactory.getLogger(MainApplication.class);

        Vertx vertx = Vertx.vertx();

        vertx.deployVerticle(new ServiceVerticle(), handler -> {
            if (handler.succeeded()) {
                log.info("Service Verticle Deployed");
            } else {
                throw new RuntimeException(handler.cause());
            }
        });
        vertx.deployVerticle(new ConsumerVerticle(), handler -> {
            if (handler.succeeded()) {
                log.info("Consumer Verticle Deployed");
            } else {
                throw new RuntimeException(handler.cause());
            }
        });
    }
}
