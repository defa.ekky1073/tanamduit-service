package com.halofina.finadvisor.utils.kafka;

/**
 * The type Message.
 */
public class KafkaPackage {
    private String type;
    private Object messege;

    /**
     * The enum Type.
     */
    public enum Type {
        /**
         * Json type.
         */
        JSON("json"),
        /**
         * Jsonarray type.
         */
        JSONARRAY("jsonarray"),
        /**
         * Jsonplus type.
         */
        JSONPLUS("jsonplus");

        private final String value;

        Type(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }
    }


    /**
     * Sets type.
     *
     * @param type the type
     */
    public void setType(Type type) {
        this.type = type.toString();
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param type the type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets messege.
     *
     * @return the messege
     */
    public Object getMessege() {
        return messege;
    }

    /**
     * Sets messege.
     *
     * @param messege the messege
     */
    public void setMessege(Object messege) {
        this.messege = messege;
    }
}
