package com.halofina.finadvisor.utils.kafka;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.kafka.admin.KafkaAdminClient;
import io.vertx.kafka.admin.NewTopic;
import io.vertx.kafka.client.consumer.KafkaConsumer;
import io.vertx.kafka.client.consumer.KafkaConsumerRecord;
import io.vertx.kafka.client.producer.KafkaProducer;
import io.vertx.kafka.client.producer.KafkaProducerRecord;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;

public abstract class KafkaUtils {
    private static Logger log = LoggerFactory.getLogger(KafkaUtils.class);
    private static KafkaAdminClient adminUtils;

    public static void publish(Vertx vertx, String url, String topic, KafkaPackage message, Handler<AsyncResult<String>> asyncResultHandler) {

        Properties props = new Properties();
        props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, url);
        props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.setProperty(ProducerConfig.ACKS_CONFIG, "1");
        
        KafkaProducer<String, String> producer = KafkaProducer.create(vertx, props);
        String stringMessage = null;

        if(message.getType().equalsIgnoreCase(KafkaPackage.Type.JSONPLUS.toString())){
            stringMessage = JsonParser.json2String((JsonObject) message.getMessege());
        } else{
            stringMessage = message.getMessege().toString();
        }

        KafkaProducerRecord<String, String> record = KafkaProducerRecord.create(topic, message.getType(), stringMessage);

        producer.send(record, handler -> {
            if (handler.succeeded()) {
                log.info("Message successfully published to topic: " + topic);

                producer.close(res -> {
                    if (res.succeeded()) {
                        asyncResultHandler.handle(Future.succeededFuture("Message successfully published to topic: " + topic));     
                    } else {
                        asyncResultHandler.handle(Future.failedFuture(res.cause()));
                    }
                });
            } else {
                log.info("Failed to send message to topic: " + topic + ", Cause: " + handler.cause().getMessage());
                asyncResultHandler.handle(Future.failedFuture(handler.cause()));
            }
        });
    }

    public static void consume(Vertx vertx, String url, String topic, String groupId, Handler<AsyncResult<KafkaConsumerRecord<String, String>>> asyncResultHandler) {

        try {
            Properties props = new Properties();
            props.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, url);
            props.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
            props.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
            props.setProperty(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
            props.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"latest");
            props.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
            props.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
            KafkaConsumer<String, String> consumer = KafkaConsumer.create(vertx, props);

            System.out.println("===========================================> Here");
            consumer.subscribe(topic, handler -> {
                if (handler.succeeded()) {
                    log.info("Success subscribing to topic: " + topic);
                } else {
                    log.info("Failed to initialize kafka consumer, cause: " + handler.cause().getMessage());
                    asyncResultHandler.handle(Future.failedFuture(handler.cause()));
                }
            });

            consumer.handler(record -> {
                asyncResultHandler.handle(Future.succeededFuture(record));
            });
        } catch (Exception e) {
            System.out.println("Caught Error: " + e);
        }
    }

    public static void createTopic(Vertx vertx, String url, String topic, Handler<AsyncResult<String>> asyncResultHandler) {
        Properties props = new Properties();
        props.setProperty(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, url);
        props.setProperty(AdminClientConfig.REQUEST_TIMEOUT_MS_CONFIG, "5000");
        adminUtils = KafkaAdminClient.create(vertx, props);

        List<NewTopic> topics = new ArrayList<>();  
        NewTopic topic1 = new NewTopic(topic, 1, (short) 1);
        topics.add(topic1);
        adminUtils.createTopics(topics, handler -> {
            if (handler.succeeded()) {
                log.info("Topic " + topic + " created"); 
                
                adminUtils.listTopics(completionHandler -> {
                    if (completionHandler.succeeded()) {
                        asyncResultHandler.handle(Future.succeededFuture(completionHandler.result().toString()));
                    } else {
                        asyncResultHandler.handle(Future.failedFuture(completionHandler.cause()));
                    }
                });
            } else {
                log.error("error while creating topics:" + handler.cause().getMessage());
                
                asyncResultHandler.handle(Future.failedFuture(handler.cause()));
            }
        });
    }

    public static void deleteTopic(Vertx vertx, String url, String topic, Handler<AsyncResult<String>> asyncResultHandler) {
        Properties props = new Properties();
        props.setProperty(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, url);
        props.setProperty(AdminClientConfig.REQUEST_TIMEOUT_MS_CONFIG, "5000");
        adminUtils = KafkaAdminClient.create(vertx, props);

        List<String> topics = new ArrayList<>();  
        topics.add(topic);
        adminUtils.deleteTopics(topics, handler -> {
            if (handler.succeeded()) {
                log.info("Topic deletion success");
                asyncResultHandler.handle(Future.succeededFuture());
            } else {
                log.info("Topic deleteion failed, cause: " + handler.cause().getMessage());
                asyncResultHandler.handle(Future.failedFuture(handler.cause()));
            }
        });
    }

}
