package com.halofina.finadvisor.utils.kafka;

public class JsonParseException extends Exception{
    public JsonParseException(String errorMessage) {
        super(errorMessage);
    }
}
