package com.halofina.finadvisor.utils.kafka;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.kafka.client.consumer.KafkaConsumerRecord;

import java.util.Arrays;

public abstract class JsonParser {

    public static JsonObject string2Json(KafkaConsumerRecord record) throws JsonParseException {
        String key = (String) record.key();

        JsonObject parsedjson = null;

        if (key.equalsIgnoreCase("json")) {
            parsedjson = new JsonObject((String) record.value());

        } else if (key.equalsIgnoreCase("jsonarray")) {
            throw new JsonParseException("Use String2JsonArray Method Instead");

        } else if (key.equalsIgnoreCase("jsonplus")) {
            JsonArray json = new JsonArray((String) record.value());
            parsedjson = new JsonObject();
            int size = json.size();
            for (int id = 0; id<size; id++){
                JsonObject elJson = json.getJsonObject(id);
                if (elJson.getString("type").equalsIgnoreCase("string")) {
                    parsedjson.put(elJson.getString("key"), elJson.getString("value"));
                } else if (elJson.getString("type").equalsIgnoreCase("int")) {
                    parsedjson.put(elJson.getString("key"), elJson.getInteger("value"));
                } else if (elJson.getString("type").equalsIgnoreCase("long")) {
                    parsedjson.put(elJson.getString("key"), elJson.getLong("value"));
                } else if (elJson.getString("type").equalsIgnoreCase("double")) {
                    parsedjson.put(elJson.getString("key"), elJson.getDouble("value"));
                } else if (elJson.getString("type").equalsIgnoreCase("boolean")) {
                    parsedjson.put(elJson.getString("key"), elJson.getBoolean("value"));
                } else if (elJson.getString("type").equalsIgnoreCase("json")) {
                    parsedjson.put(elJson.getString("key"), new JsonObject(elJson.getString("value")));
                } else if (elJson.getString("type").equalsIgnoreCase("jsonarray")) {
                    parsedjson.put(elJson.getString("key"), new JsonArray(elJson.getString("value")));
                } else if (elJson.getString("type").equalsIgnoreCase("array")) {
                    parsedjson.put(elJson.getString("key"), new JsonArray(elJson.getString("value")));
                }
            }
        }
        return parsedjson;
    }


    public static JsonArray string2JsonArray(KafkaConsumerRecord record) throws JsonParseException {
        String key = (String) record.key();

        JsonArray parsedjson;

        if (key.equalsIgnoreCase("jsonarray")) {
            parsedjson = new JsonArray((String) record.value());

        } else {
            throw new JsonParseException("Use String2Json Method Instead");
        }
        return parsedjson;
    }

    public static String json2String(JsonObject json) {
        JsonArray jsonparse = new JsonArray();
        json.forEach(el -> {
            if(el.getValue().getClass().getName().equalsIgnoreCase(String.class.getName())){
                JsonObject entryJson = new JsonObject();
                entryJson.put("type","string");
                entryJson.put("key",el.getKey());
                entryJson.put("value",el.getValue());
                jsonparse.add(entryJson);
            }
            else if(el.getValue().getClass().getName().equalsIgnoreCase(Integer.class.getName())){
                JsonObject entryJson = new JsonObject();
                entryJson.put("type","int");
                entryJson.put("key",el.getKey());
                entryJson.put("value",el.getValue());
                jsonparse.add(entryJson);
            }
            else if(el.getValue().getClass().getName().equalsIgnoreCase(Long.class.getName())){
                JsonObject entryJson = new JsonObject();
                entryJson.put("type","long");
                entryJson.put("key",el.getKey());
                entryJson.put("value",el.getValue());
                jsonparse.add(entryJson);
            }
            else if(el.getValue().getClass().getName().equalsIgnoreCase(Double.class.getName())){
                JsonObject entryJson = new JsonObject();
                entryJson.put("type","double");
                entryJson.put("key",el.getKey());
                entryJson.put("value",el.getValue());
                jsonparse.add(entryJson);
            }
            else if(el.getValue().getClass().getName().equalsIgnoreCase(Boolean.class.getName())){
                JsonObject entryJson = new JsonObject();
                entryJson.put("type","boolean");
                entryJson.put("key",el.getKey());
                entryJson.put("value",el.getValue());
                jsonparse.add(entryJson);
            }
            else if(el.getValue().getClass().getName().equalsIgnoreCase(JsonObject.class.getName())){
                JsonObject entryJson = new JsonObject();
                entryJson.put("type","json");
                entryJson.put("key",el.getKey());
                entryJson.put("value",el.getValue().toString());
                jsonparse.add(entryJson);
            }
            else if(el.getValue().getClass().getName().equalsIgnoreCase(JsonArray.class.getName())){
                JsonObject entryJson = new JsonObject();
                entryJson.put("type","jsonarray");
                entryJson.put("key",el.getKey());
                entryJson.put("value",el.getValue().toString());
                jsonparse.add(entryJson);
            }
            else if(el.getValue().getClass().getName().equalsIgnoreCase(Arrays.class.getName())){
                JsonObject entryJson = new JsonObject();
                entryJson.put("type","array");
                entryJson.put("key",el.getKey());
                entryJson.put("value",el.getValue().toString());
                jsonparse.add(entryJson);
            }
        });

        return jsonparse.toString();
    }
}
