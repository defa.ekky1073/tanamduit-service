SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE TABLE `invest_product_mutual_fund`;
INSERT INTO `invest_product_mutual_fund`
VALUES (1, 1, 1, 1, 'https://drive.google.com/file/d/1entjwL-GajLl4ufHpsB0CRS1goBJZWnA/preview',
        'https://drive.google.com/file/d/1SXMoTtWmE-S1XngxBKYZbpH4E2ay75w0/preview', 45.21, 100000.00, 100000.00,
        100000.00, 68.87, 98736.00, 0, 0, '2019-07-21', 1452.080000, 1452.080000, 3360000.00, 0.49, 1.46, 2.86,
        5.39, 2.65, 45.21, NULL, '2019-07-20 22:17:20', NULL, NULL, 0);
INSERT INTO `invest_product_mutual_fund`
VALUES (2, 2, 1, 2, 'https://drive.google.com/file/d/1mf9G1jGJT3VYr5Iser5_HdsVuHrTPEU-/preview',
        'https://drive.google.com/file/d/1DsNaI7_EvAk0fTJGpGracLztRVb0FDQE/preview', 15.56, 100000.00, 100000.00,
        100000.00, 50.00, 57750.00, 1, 1, '2019-07-21', 1155.550000, 1155.550000, 44670.00, 3.57, 3.40, 6.59,
        5.27, 5.68, 15.56, NULL, '2019-07-20 22:21:21', NULL, NULL, 0);

TRUNCATE TABLE `invest_product_mutual_fund_disburse_bank`;
INSERT INTO `invest_product_mutual_fund_disburse_bank`
VALUES (1, 1, 2, 'BMRIIDJA', 'Bank Mandiri', '1040004890351', 'Bahana MES Syariah Fund',
        'https://docs.google.com/uc?id=1Dem0v8X-UfCIhW22LUzRprj6aVduWTA1', 1, '2019-07-20 22:24:43', NULL, NULL, 0);
INSERT INTO `invest_product_mutual_fund_disburse_bank`
VALUES (2, 1, 6, 'CENAIDJA', 'Bank BCA', '4582557245', 'Bahana MES Syariah Fund',
        'https://docs.google.com/uc?id=1yHmz_zq6mpzrPxXqxyACzqJrQIL8nwu6', 0, '2019-07-20 22:25:18', NULL, NULL, 0);
INSERT INTO `invest_product_mutual_fund_disburse_bank`
VALUES (3, 2, 2, 'BMRIIDJA', 'Bank Mandiri', '1040004889486', 'Batavia Dana Kas Maxima',
        'https://docs.google.com/uc?id=1Dem0v8X-UfCIhW22LUzRprj6aVduWTA1', 1, '2019-07-20 22:24:43', NULL, NULL, 0);
INSERT INTO `invest_product_mutual_fund_disburse_bank`
VALUES (4, 2, 6, 'CENAIDJA', 'Bank BCA', '4582556397', 'Batavia Dana Kas Maxima',
        'https://docs.google.com/uc?id=1yHmz_zq6mpzrPxXqxyACzqJrQIL8nwu6', 0, '2019-07-20 22:25:18', NULL, NULL, 0);

TRUNCATE TABLE `invest_product_mutual_fund_history`;
INSERT INTO `invest_product_mutual_fund_history`
VALUES (1, 1, 2, 1, 1, 45.21, 100000.00, 100000.00, 100000.00, 68.87, 98736.00, 0, 0, '2019-07-20 00:14:51', 1442.080000,
        1442.080000, 3360000.00, 0.49, 1.46, 2.86, 5.39, 2.65, 45.21, NULL, '2019-07-20', NULL, NULL, 0);
INSERT INTO `invest_product_mutual_fund_history`
VALUES (2, 1, 2, 1, 1, 45.21, 100000.00, 100000.00, 100000.00, 68.87, 98736.00, 0, 0,'2019-07-21 00:14:51', 1452.080000, 
        1452.080000, 3360000.00, 0.49, 1.46, 2.86, 5.39, 2.65, 45.21, NULL, '2019-07-21', NULL, NULL, 0);
INSERT INTO `invest_product_mutual_fund_history`
VALUES (3, 2, 2, 2, 1, 45.21, 100000.00, 100000.00, 100000.00, 68.87, 98736.00, 0, 0, '2019-07-20 00:14:51', 1105.550000,
        1105.550000, 44670.00, 3.57, 3.40, 6.59, 5.27, 5.68, 15.56, NULL, '2019-07-20', NULL, NULL, 0);
INSERT INTO `invest_product_mutual_fund_history`
VALUES (4, 2, 2, 2, 1, 45.21, 100000.00, 100000.00, 100000.00, 68.87, 98736.00, 0, 0, '2019-07-21 00:14:51', 1155.550000, 
        1155.550000, 44670.00, 0.49, 1.46, 2.86, 5.39, 2.65, 45.21, NULL, '2019-07-21', NULL, NULL, 0);

        TRUNCATE TABLE `invest_product_general`;
INSERT INTO `invest_product_general`
VALUES (1, 4, 1, 'GOLDINDOGOLD', 'Emas Antam', 'GOLDIDGD', 'GOLD',
        'http://halofina.id/img/product-partner/icon_indogold.png', 'Emas Indogold', 1, 1, '2019-07-20 22:05:00', NULL,
        NULL, 0);
INSERT INTO `invest_product_general`
VALUES (2, 4, 2, 'GOLDTAMASIA', 'Emas Antam', 'GOLDTMSA', 'GOLD',
        'http://halofina.id/img/product-partner/icon_indogold.png', 'Emas Tamasia', 1, 1, '2019-07-20 22:05:00', NULL,
        NULL, 0);
INSERT INTO `invest_product_general`
VALUES (3, 1, 1, 'BZ002MMCKASMAX00', 'Batavia Dana Kas Maxima', 'BDKM', 'MUTUALFUND',
        'https://cdn.tanamduit.com/images/product/BATAVIA.png', 'Batavia Dana Kas Maxima', 1, 0, '2019-07-20 22:18:01',
        NULL, NULL, 0);
INSERT INTO `invest_product_general`
VALUES (4, 2, 2, 'DX002FISBMSYAR00', 'Bahana MES Syariah Fund', 'BMSF', 'MUTUALFUND',
        'https://cdn.tanamduit.com/images/product/BAHANA.png', 'Bahana MES Syariah Fund', 1, 1, '2019-07-20 22:22:21',
        NULL, NULL, 0);

SET FOREIGN_KEY_CHECKS = 1;
