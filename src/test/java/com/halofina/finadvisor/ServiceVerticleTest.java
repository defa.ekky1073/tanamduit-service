package com.halofina.finadvisor;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import com.halofina.finadvisor.utils.kafka.KafkaPackage;
import com.halofina.finadvisor.utils.kafka.KafkaUtils;

import org.flywaydb.core.Flyway;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import id.halofina.roboadvisor.core.database.migration.FlywayConfiguration;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class ServiceVerticleTest {
    private static final String migrationTableName = "_flyway_schema_history_service_tanamduit_test";
    private static Flyway flyway;
    private static Vertx vertx;
    private static URI dbUri;
    private static String url = "ec2-18-136-123-232.ap-southeast-1.compute.amazonaws.com:9092";
    
    @BeforeClass
    public static void __beforeClass(TestContext context) throws URISyntaxException {
        Async async = context.async();

        vertx = Vertx.vertx();

        dbUri = new URI(System.getenv("ROBO_INTEGRATION_TANAMDUIT_DB"));

        Future<Void> future1 = Future.future();
        vertx.deployVerticle(new ServiceVerticle(), deployHandler -> {
            context.assertTrue(deployHandler.succeeded(), deployHandler.cause() == null ? "" : deployHandler.cause().getMessage());

            FlywayConfiguration flywayConfig = new FlywayConfiguration(dbUri,
                "classpath:db/finadvisor", 
                migrationTableName);
            flyway = Flyway.configure().configuration(flywayConfig.toMap()).load();
            flyway.migrate();

            future1.complete();
        });

        Future<Void> future2 = Future.future();
        vertx.deployVerticle(new ConsumerVerticle(), deployHandler -> {
            future2.complete();
        });

        CompositeFuture.all(future1, future2).setHandler(compositeHandler -> {
            async.complete();
        });
    }

    @Test
    public void updateNavTest(TestContext context) {
        final Async async = context.async();

        JsonObject data = new JsonObject()
            .put("product_code", "BZ002MMCKASMAX00");

        KafkaPackage message = new KafkaPackage();
        message.setType(KafkaPackage.Type.JSON);
        message.setMessege(data);

        KafkaUtils.publish(vertx, url, ConsumerVerticle.TOPIC_UPDATE_PRODUCT_NAV, message, sendHandler -> {
            context.assertTrue(sendHandler.succeeded(), sendHandler.cause() == null ? "" : sendHandler.cause().getMessage());

            System.out.println("Message Published");
            async.complete();
        });
    }
}